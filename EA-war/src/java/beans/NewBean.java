/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import Model.Airport;
import Model.Plane;
import ejb.NSInterface;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author 1
 */
@Stateful
@Named(value = "bean")
@ConversationScoped
public class NewBean implements NewInterface{
    
    @EJB 
    private NSInterface ni;
    private Airport airport;
    private Plane plane;
    private List<Plane> list;
    @Inject Conversation conv;
    
    private String name;
    private String place;
    private int speed;
    private int high;
    private String air;
    
    @PostConstruct
    private void postConstract() {
        conv.begin();
    }
    
    @Override
    public String addPlane(){
        plane = new Plane();
        plane.setName(name);
        plane.setSpeed(speed);
        plane.setHigh(high);
        plane.setAirport(ni.SelectAirport(air));
        ni.AddPlane(plane);
        return "index";       
    }
    
    @Override
    public String addAirport(){
        Airport airp = new Airport();
        airp.setName(name);
        airp.setPlace(place);
        ni.AddAirport(airp);
        return "index";       
    }
    
    @Override
    public String showPlanes(){
        setList(ni.ShowPlanes());
        return "index";  
    }

    /**
     * @return the ni
     */
    @Override
    public NSInterface getNi() {
        return ni;
    }

    /**
     * @param ni the ni to set
     */
    @Override
    public void setNi(NSInterface ni) {
        this.ni = ni;
    }

    /**
     * @return the airport
     */
    @Override
    public Airport getAirport() {
        return airport;
    }

    /**
     * @param airport the airport to set
     */
    @Override
    public void setAirport(Airport airport) {
        this.airport = airport;
    }

    /**
     * @return the plane
     */
    @Override
    public Plane getPlane() {
        return plane;
    }

    /**
     * @param plane the plane to set
     */
    @Override
    public void setPlane(Plane plane) {
        this.plane = plane;
    }

    /**
     * @return the list
     */
    @Override
    public List<Plane> getList() {
        setList(ni.ShowPlanes());
        return list;
    }

    /**
     * @param list the list to set
     */
    @Override
    public void setList(List<Plane> list) {
        this.list = list;
    }

    /**
     * @return the name
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    @Override
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the place
     */
    @Override
    public String getPlace() {
        return place;
    }

    /**
     * @param place the place to set
     */
    @Override
    public void setPlace(String place) {
        this.place = place;
    }

    /**
     * @return the speed
     */
    @Override
    public int getSpeed() {
        return speed;
    }

    /**
     * @param speed the speed to set
     */
    @Override
    public void setSpeed(int speed) {
        this.speed = speed;
    }

    /**
     * @return the high
     */
    @Override
    public int getHigh() {
        return high;
    }

    /**
     * @param high the high to set
     */
    @Override
    public void setHigh(int high) {
        this.high = high;
    }

    /**
     * @return the air
     */
    @Override
    public String getAir() {
        return air;
    }

    /**
     * @param air the air to set
     */
    @Override
    public void setAir(String air) {
        this.air = air;
    }
    
    
    
    
    
}
