/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package beans;

import Model.Airport;
import Model.Plane;
import ejb.NSInterface;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author 1
 */
@Local
public interface NewInterface {
    
    public String addPlane();
    public String addAirport();
    public String showPlanes();
  
    public NSInterface getNi();
    public void setNi(NSInterface ni);
    public Airport getAirport();
    public void setAirport(Airport airport);
    public Plane getPlane();
    public void setPlane(Plane plane);
    public List<Plane> getList();
    public void setList(List<Plane> list);
    public String getName();
    public void setName(String name);
    public String getPlace();
    public void setPlace(String place);
    public int getSpeed();
    public void setSpeed(int speed);
    public int getHigh();
    public void setHigh(int high);
    public String getAir();
    public void setAir(String air);
}
