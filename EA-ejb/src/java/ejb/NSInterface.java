/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejb;

import Model.Airport;
import Model.Plane;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author 1
 */
@Remote
public interface NSInterface {
    
    public void AddPlane(Plane plane);
    public void AddAirport(Airport airport);
    public List<Plane> ShowPlanes();
    public Airport SelectAirport(String air);
    
}
